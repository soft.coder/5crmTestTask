import angular from 'angular';
import Cart from './cart.factory';
import utils from '../../utils/utils';

let cart = angular.module('Cart', [utils])

.factory('Cart', Cart)

.name;

export default cart;
