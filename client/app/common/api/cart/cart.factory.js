function CartFactory($q, utils, Product) {
  "ngInject";
  let products, cart;
  let saveId = 1;
  if(!(cart = utils.getFromStorage("cart"))){
    cart = [];
    utils.saveToStorage("cart", cart);

  } else if(cart.length){
    saveId = cart[cart.length - 1].id + 1;
  }
  let save = () => {
    utils.saveToStorage("cart", cart);
  };

  let query = () => {
    return $q.resolve(cart);
  };
  let changeCount = (productId, count) => {
    return Product.query().then(products => {
      let cartItem, product;
      if(product = products.find(p => p.id === productId)) {
        if (cartItem = cart.find(c => c.productId === productId)) {
          if(count >= 0 && count <= product.count) {
            cartItem.count = count;
            save();
            return cartItem
          } else {
            return $q.reject(`count:${count} exceed`)
          }
        } else {
          let newCartItem = {
            id: saveId++,
            productId: product.id,
            count: count
          };
          cart.push(newCartItem);
          save();
          return newCartItem;
        }
      } else {
        return $q.reject(`no found productId:${productId}`);
      }

    });
  };
  return {
    query,
    changeCount
  }
}

export default CartFactory;
