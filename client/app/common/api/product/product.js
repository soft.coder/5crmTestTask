import angular from 'angular';
import Product from './product.factory';
import utils from '../../utils/utils';


let product = angular.module('Product', [utils])

.factory('Product', Product)

.name;

export default product;
