import imgUrl1 from '../../../../assets/product1.jpeg'
import imgUrl2 from '../../../../assets/product2.jpeg'
import imgUrl3 from '../../../../assets/product3.jpeg'
import imgUrl4 from '../../../../assets/product4.png'
import imgUrl5 from '../../../../assets/product5.jpeg'
import imgUrl6 from '../../../../assets/product6.jpeg'
import imgUrl7 from '../../../../assets/product7.jpeg'

function ProductFactory($q, utils) {
  "ngInject";
  let products;
  let generateProducts = count => {
    let products = [];

    let imgs = [imgUrl1, imgUrl2, imgUrl3, imgUrl4, imgUrl5, imgUrl6, imgUrl7];

    for (let i = 0; i < count; i++) {
      products.push({
        id: i + 1,
        name: `Product name ${i + 1}`,
        count: utils.getRandom(1, 10),
        cost: utils.getRandom(100, 1000),
        imgUrl: imgs[utils.getRandom(0, 7)]
      });
    }
    return products;
  };
  if(!(products = utils.getFromStorage("products"))){
    products = generateProducts(30);
    utils.saveToStorage("products", products);

  }

  let query = () => {
    return $q.resolve(products);
  };
  let get = id => {
    return $q((resolve, reject) => {
      let result = products.find(p => p.id === id);
      if(result) {
        return resolve(result);
      } else {
        return reject(result);
      }
    });
  };
  let save = product => {
    return $q((resolve, reject) => {
      let result = products.some(p => {
        if (p.id === product.id) {
          p.name = product.name;
          p.count = product.count;
          p.cost = product.cost;
          return true;
        }
      });
      if (!result) {
        reject();
      } else {
        utils.saveToStorage("products", products);
        resolve();
      }
    });
  };

  return {
    query,
    get,
    save
  }
}

export default ProductFactory;
