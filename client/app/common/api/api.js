import angular from 'angular';
import ApiFactory from './api.factory';
import Cart from './cart/cart';
import Product from './product/product';

let api = angular.module('Api', [Product, Cart])

.factory('Api', ApiFactory)

.name;

export default api;
