let ApiFactory = function ($q, Product, Cart) {
  "ngInject";
  let getAllProduct = () => {
    return $q.all([Product.query(), Cart.query()]).then(values => {
      let [products, cart] = values;
      return $q.resolve(products.map(p => {
        let cartItem = cart.find(c => c.productId === p.id);
        return Object.assign({}, p, {countInCart: cartItem ? cartItem.count : 0});
      }));
    });
  };
  let getAllProductInCart = () => {
    return getAllProduct().then(products => products.filter(p => p.countInCart));
  };
  let changeCountInCart = (product, count) => {
    if(count >= 0 && count <= product.count) {
      product.countInCart = count;
      let lastCountInCart = product.countInCart;
      return Cart.changeCount(product.id, count).catch(reason => {
        product.countInCart = lastCountInCart;
        console.error(`AddToCart: ${reason}`);
      });
    } else {
      $q.reject(`count:${count} exceed`);
    }
  };
  let addToCart = product => changeCountInCart(product, product.countInCart + 1);
  let removeFromCart = product => changeCountInCart(product, product.countInCart - 1);

  return {
    getAllProduct,
    getAllProductInCart,
    addToCart,
    removeFromCart
  };
};

export default ApiFactory;
