import angular from 'angular';
import UtilsFactory from './utils.factory';

let utilsModule= angular.module('utils', [])

.factory('utils', UtilsFactory)

.name;

export default utilsModule;
