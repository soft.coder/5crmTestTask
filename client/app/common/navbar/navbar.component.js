import template from './navbar.html';
import controller from './navbar.controller';
import './navbar.less';

let navbarComponent = {
  bindings: {},
  template,
  controller
};

export default navbarComponent;
