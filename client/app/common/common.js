import angular from 'angular';
import Navbar from './navbar/navbar';
import Api from './api/api';

let commonModule = angular.module('app.common', [
  Navbar,
  Api
])

.name;

export default commonModule;
