import angular from 'angular';
import Cart from './cart/cart';
import CartItem from './cart/cartItem/cartItem';
import ProductList from './productList/productList';
import Product from './productList/product/product';

let componentModule = angular.module('app.components', [
  Cart,
  CartItem,
  ProductList,
  Product
])

.name;

export default componentModule;
