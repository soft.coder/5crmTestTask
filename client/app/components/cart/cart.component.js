import template from './cart.html';
import controller from './cart.controller';
import './cart.less';

let cartComponent = {
  bindings: {
    cart: '<'
  },
  template,
  controller
};

export default cartComponent;
