class CartController {
  constructor() {
    "ngInject"
    // Api.getAllProductInCart().then(cart => this.cart = cart);
    this.getTotalCost = () => {
      if (this.cart) {
        let totalCost = 0;
        this.cart.forEach(cartItem => {
          totalCost += cartItem.countInCart * cartItem.cost;
        });
        return totalCost;
      }
    }
  }
}

export default CartController;
