import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import cartComponent from './cart.component';
import cartItem from './cartItem/cartItem';

let cartModule = angular.module('crm.cart', [
  uiRouter,
  cartItem
])

.component('crmCart', cartComponent)

.name;

export default cartModule;
