import template from './cartItem.html';
import controller from './cartItem.controller';
import './cartItem.less';

let cartItemComponent = {
  bindings: {
    cartItem: '<'
  },
  template,
  controller
};

export default cartItemComponent;
