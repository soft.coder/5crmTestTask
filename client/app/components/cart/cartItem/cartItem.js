import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import cartItemComponent from './cartItem.component';

let cartItemModule = angular.module('crm.cartItem', [
  uiRouter
])

.component('crmCartItem', cartItemComponent)

.name;

export default cartItemModule;
