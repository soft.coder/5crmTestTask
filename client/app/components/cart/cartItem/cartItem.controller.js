
class CartItemController {
  constructor(Api) {
    "ngInject";

    this.addToCart = () => {
      this.btnCartDisabled = true;
      return Api.addToCart(this.cartItem).finally(() => {
        this.btnCartDisabled = false;
      });
    };
    this.removeFromCart = () => {
      this.btnCartDisabled = true;
      return Api.removeFromCart(this.cartItem).finally(() => {
        this.btnCartDisabled = false;
      });
    };

  }
}

export default CartItemController;
