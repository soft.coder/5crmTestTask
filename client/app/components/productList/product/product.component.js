import template from './product.html';
import controller from './product.controller';
import './product.less';

let productComponent = {
  bindings: {
    product: "<"
  },
  template,
  controller
};

export default productComponent;
