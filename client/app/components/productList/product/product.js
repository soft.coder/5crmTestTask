import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import productComponent from './product.component';

let productModule = angular.module('crmProduct', [
  uiRouter
])

.component('crmProduct', productComponent)

.name;

export default productModule;
