
class ProductController {
  constructor(Api) {
    "ngInject"

    this.addToCart = () => {
      this.btnCartDisabled = true;
      return Api.addToCart(this.product).finally(() => this.btnCartDisabled = false);
    };
    this.removeFromCart = () => {
      this.btnCartDisabled = true;
      return Api.removeFromCart(this.product).finally(() => this.btnCartDisabled = false);
    };
  }
}

export default ProductController;
