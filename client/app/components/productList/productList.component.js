import template from './productList.html';
import controller from './productList.controller';
import './productList.less';

let productListComponent = {
  bindings: {
    products: '<'
  },
  template,
  controller: controller
};

export default productListComponent;
