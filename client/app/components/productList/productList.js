import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import productListComponent from './productList.component';

let productListModule = angular.module('crm.productList', [
  uiRouter
])

.component('crmProductList', productListComponent)

.name;

export default productListModule;
