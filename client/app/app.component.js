import template from './app.html';
import './app.less';

let appComponent = {
  template,
  controller: function($scope){
    "ngInject"
    $scope.$on('global:search', (event, txtSearch) => {
      console.log('root search', txtSearch, event);
      $scope.$broadcast('global:search', txtSearch);
    })
  }
};

export default appComponent;
