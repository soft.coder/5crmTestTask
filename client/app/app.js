import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import ngAnimate from 'angular-animate';
import ngTouch from 'angular-touch';
import ngBootstrap from 'angular-ui-bootstrap';
// import ngBootstrapButton from 'angular-ui-bootstrap/src/buttons'
import 'bootstrap/dist/css/bootstrap.css'
import 'angular-ui-bootstrap/dist/ui-bootstrap-csp.css';
import 'normalize.css';

angular.module('app', [
    uiRouter,
    Common,
    Components,
    ngAnimate,
    ngTouch,
    ngBootstrap
  ])
  .config(($locationProvider, $urlRouterProvider, $stateProvider) => {
    "ngInject";
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');


    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('productList', {
        url: '/',
        component: 'crmProductList',
        resolve: {
          products: Api => {
            return Api.getAllProduct();
          }
        }
      })
      .state('cart', {
        url: '/cart',
        component: 'crmCart',
        resolve: {
          cart: Api => {
            return Api.getAllProductInCart();
          }
        }
    });
  })

  .component('app', AppComponent);
